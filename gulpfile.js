'use strict';

// Aqui nós carregamos o gulp e os plugins através da função `require` do nodejs
var gulp      = require('gulp'),
uglify        = require('gulp-uglify'),
sass          = require('gulp-sass'),
cleanCSS      = require('gulp-clean-css'),
autoprefixer  = require('gulp-autoprefixer'),
include       = require("gulp-include"),
minifycss     = require('gulp-minify-css');
require('es6-promise').polyfill();

var paths = {
  scss: './assets/scss/*.scss',
  images: './public/images/sprites/icones/*.png',
  javascript: "assets/js/*.js"
};

gulp.task('javascript', function() {
  gulp.src(paths.javascript)
    .pipe(include())
    .pipe(uglify().on('error', function(e){
      console.log(e);
    }))
    .pipe(gulp.dest('./public/js'));
});

gulp.task('styles', function () {
  gulp.src(paths.scss)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(minifycss())
    // .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./public/css'));
});

// Default task
gulp.task('default', function() {
  gulp.start('styles', 'javascript');
});

gulp.task('watch', ['styles', 'javascript'], function() {
  gulp.watch(paths.scss, ['styles']);
  gulp.watch(paths.javascript, ['javascript']);
});
