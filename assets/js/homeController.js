var homeController = [
  '$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location) {
    //lista de clientes
    $scope.clients = null;

    $scope.loadAll = function(){
      $scope.showLoader();
      $http({
        method: 'GET',
        url: $scope.server("/Cliente")
      }).
      success(function(data, status, headers, config) {
        $scope.data = data;
        $scope.clients = Object.keys($scope.data[0]);
        $scope.order = {};
        $scope.currentSort = {};    
      });
    }
    $scope.delete = function(client){
      $http({
        method: 'DELETE',
        url: $scope.server("/Cliente/"+client.id)
      }).
      success(function(data, status, headers, config) {
        var index = $scope.clients.indexOf(client);
        $scope.clients.splice(index, 1);
      });
      $scope.$apply();
    }

    $scope.order_by = function(column){
      if ($scope.order[column] == column) {
          $scope.order[column+'Desc'] = !$scope.order[column+'Desc'];
      } else {
          $scope.order[column] = column;
          $scope.order[column+'Desc'] = false;
      }
      $scope.currentSort = {exp: $scope.order[column], reverse: $scope.order[column+'Desc']} ;
    };
  }
];
