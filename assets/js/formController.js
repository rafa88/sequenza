var formController = [
  '$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location) {
    $scope.client = null;

    $scope.edit = function(){
      $scope.showLoader();
      if( !$routeParams.id ) return false;
      $http({
        method: 'GET',
        url: $scope.server("/Cliente/"+$routeParams.id)
      }).
      success(function(data, status, headers, config) {
        $scope.client = data;
      });
    }

    // Create client
    $scope.save_client = function($event){
      if ($scope.clientForm.$invalid) {
        var error = document.querySelector(".alert-danger");
        error.className += "show in";
        error.innerHTML = "Preencha o formulário.";
        return false;
      }
      var method = !$routeParams.id ? "POST" : "PUT";
      var url = !$routeParams.id ? "/Cliente/" : "/Cliente/"+$routeParams.id;
      $http({
        method: method,
        url: $scope.server(url),
        data: $scope.client
      }).
      success(function(data, status, headers, config) {
        document.querySelector(".alert-success").className += "show in";
      });
    }
  }
];
