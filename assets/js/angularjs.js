SERVER_URL = "http://dev04.sequenza.com.br:9090/api";

var app = angular.module('app',['ngResource', 'ui.bootstrap']);

app.config([
  "$routeProvider", "$locationProvider", "$httpProvider", function($routeProvider, $locationProvider, $httpProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!');

    $routeProvider.
    when('/',{
      templateUrl:'view/home.html',
      controller:homeController
    }).
    when('/cadastrar',{
      templateUrl:'view/form.html',
      controller:formController
    }).
    when('/editar/:id',{
      templateUrl:'view/form.html',
      controller:formController
    });

    $httpProvider.responseInterceptors.push([
      '$q', '$rootScope', function($q, $rootScope) {
        return function(promise) {
          $rootScope.hideLoader();
          return promise.then(function(response) {
            return(response);
          }, function(response) {
            $data = response.data;
            $error = $data.error;
            if ($error && $error.text)
              alert("ERROR: " + $error.text);
            else{
              if (response.status=404){
                var error = document.querySelector(".alert-danger");
                error.className += "show in";
                error.innerHTML = "Erro ao acessar servidor. Página não encontrada. Veja o log de erros para maiores detalhes";
              }else{
                alert("ERROR! See log console");
              }
            }
            return $q.reject(response);
          });
        }
      }
    ]);
  }
]);

app.run([
  "$rootScope", function($rootScope) {
    //Uma flag que define se o ícone de acesso ao servidor deve estar ativado
    $rootScope.showLoaderFlag = false;
    //Força que o ícone de acesso ao servidor seja ativado
    $rootScope.showLoader=function(){
      $rootScope.showLoaderFlag=true;
    }
    //Força que o ícone de acesso ao servidor seja desativado
    $rootScope.hideLoader=function(){
      $rootScope.showLoaderFlag=false;
    }
    //Método que retorna a URL completa de acesso ao servidor. 
    // Evita usar concatenação
    $rootScope.server=function(url){
      return SERVER_URL + url;
    }
  }
]);

app.filter('startFrom', function() {
  return function(input, start) {
    if (input==null)
      return null;
    start = +start;
    return input.slice(start);
  }
});

app.directive('phoneInput', ['$filter', '$browser', function($filter, $browser) {
  return {
    require: 'ngModel',
    link: function($scope, $element, $attrs, ngModelCtrl) {
      var listener = function() {
        var value = $element.val().replace(/[^0-9]/g, '');
        $element.val($filter('tel')(value, false));
      };

      ngModelCtrl.$parsers.push(function(viewValue) {
        if(!viewValue) return false;
        return viewValue.replace(/[^0-9]/g, '').slice(0,12);
      });

      ngModelCtrl.$render = function() {
        $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
      };

      $element.bind('change', listener);
      $element.bind('keydown', function(event) {
        var key = event.keyCode;
        if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
          return;
        }
        $browser.defer(listener); // Have to do this or changes don't get picked up properly
      });

      $element.bind('paste cut', function() {
        $browser.defer(listener);
      });
    }
  };
}]);
app.filter('tel', function () {
  return function (tel) {
    if (!tel) { return ''; }

    var city, num1, num2, num3;

    if(tel.length <= 10){
      city = tel.slice(0, 2);
      num1 = tel.slice(2,6);
      num2 = tel.slice(6,10);
    }else{
      city = tel.slice(0, 2);
      num1 = tel.slice(2,7);
      num2 = tel.slice(7,11);
    }
    if(city && num1 && num2 && num3){
      return ("(" + city + ") " + num1 +"-" + num2 + "-" + num3).trim();
    }
    else if(city && num1 && num2) {
      return ("(" + city + ") " + num1 +"-" + num2).trim();
    }else if(city && num1) {
      return ("(" + city + ") " + num1).trim();
    }else if(city) {
      return ("(" + city ).trim();
    }

  };
});

app.directive('confirm', ['$parse', '$rootScope', function($parse, $rootScope) {
  return {
    priority: -1,
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.unbind("click").bind("click", function() {
        $(".modal .modal-footer").find("[name='yes']").unbind('click.confirm').bind("click.confirm", function() {
          $parse(attrs.ngClick)(scope);
        });
        $rootScope.$broadcast('confirm', attrs.confirm);
      });
    }
  }
}]);

app.directive('confirmBox', ['$parse', function($parse) {
  return {
    restrict: 'A',
    template: '<div class="modal-center">'+
    '<div class="modal-dialog modal-sm">' +
      '<div class="modal-content">' +
        '<div class="modal-header">' +
          '<h4 class="modal-title" id="mySmallModalLabel">Desaja excluir esse cliente</h4>' +
        '</div>'+
        '<div class="modal-body">Tem certeza que deseja excluir esse cliente</div>'+
        '<div class="modal-footer">' +
          '<button type="button" class="btn btn-default" name="no">Cancelar</button>' +
          '<button type="button" class="btn btn-primary" name="yes" style="margin-left:5px;">Excluir</button></div>'+
        '</div>'+
      '</div>' +
    '</div>',
    link: function(scope, element, attrs){
      element.css("display","none");
      element.addClass('modal');

      var noBtn = element.find("[name='no']");
      noBtn.bind("click", function() {
        element.css("display","none");
      });
      var yesBtn = element.find("[name='yes']");
      yesBtn.bind("click", function() {
        element.hide();
      });

      scope.$on("confirm", function(event, text) {
        element.find(".body").html(text);
        element.show();
      });
    }
  }
}]);

app.directive('ifLoading', ['$http', function($http) {
  return {
    restrict: 'A',
    link: function(scope, elem) {
      scope.isLoading = isLoading;
      scope.$watch(scope.isLoading, toggleElement);
      function toggleElement(loading) {
        if (loading) {
          elem[0].style.display = "block";
        } else {
          elem[0].style.display = "none";
        }
      }
      function isLoading() {
        return $http.pendingRequests.length > 0;
      }
    }
  }
}]);

