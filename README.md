# Instruções para instalação e exceção do site

## instalação

1. Para acessar o projeto precisa ter Apache instalado, caso não tenha pode instalar o MAMP
2. Acesse a pasta public para acessar o projeto

## instalação
1. Caso precise compilar de novo
2. Verifique que node.js esta instalado, caso não esteja instalado rode esse comando (`brew install node`)
3. Entre na pasta do projeto e excecute esses comandos `npm install` (ele instala as dependencia do node)
4. Excecute o comando `gulp watch` para comprimir e gerar os CSS e JavaScript
