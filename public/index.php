<?php include "php/url.php"; ?>
<!DOCTYPE html>
<html lang="pt-br" ng-app="app">
<head>
  <base href="<?php echo $pageURL ?>">
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Teste Front-End</title>
  <link rel="stylesheet" type="text/css" href="css/main.css">

  <script src="js/libs/jquery-3.2.1.js"></script>
  <script src="js/libs/bootstrap.js"></script>
  <script src="js/libs/angular.min.js"></script>
  <script src="js/libs/angular-locale_pt-br.js"></script>
  <script src="js/libs/angular-resource.min.js"></script>
  <script src="js/libs/ui-bootstrap-tpls-0.10.0.js"></script>
  
  <script src="js/libs/ui-utils.js"></script>

  <script src="js/angularjs.js"></script>
  <script src="js/homeController.js"></script>
  <script src="js/formController.js"></script>
</head>
<body>
  <main class="container">
    <header id="header" class="header">
      <div class="logo"><a href="./" title=""><img src="images/logo.png" alt=""></a></div>
      <h1>Sistema de cadastro de cliente</h1>
      <div class="sign">
        <div class="user">
          <i class="fa fa-user" aria-hidden="true"></i>
          <a href="#" title="">user@sequenza.com.br</a><br>
        </div>
        <a href="#" title="" class="logout">Sair</a>
      </div>
    </header><!-- /header -->
    <div class="loader" if-loading>
      <div class="loader loader--style3" title="2">
        <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
        <path fill="#337ab7" d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z" transform="rotate(80 25 25)">
          <animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 25 25" to="360 25 25" dur="0.6s" repeatCount="indefinite"></animateTransform>
          </path>
        </svg>
      </div>
    </div>
    <div ng-view></div>
  </main>

</body>
</html>